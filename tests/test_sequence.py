# -*- coding: utf-8 -*-

import os
from tempfile import TemporaryDirectory
import unittest
import itertools

import numpy as np

from evoclearn.core import Sequence, Sequences, DEFAULT_VARIABLE_PARAMS
from evoclearn.core import io
from evoclearn.core import samplers


FIXTURES_DIR = os.path.join(os.path.dirname(__file__), "fixtures")


class TestSequences(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls.seed = 777
        cls.numseqs = 50
        cls.labs = ["C", "V"]
        cls.params = DEFAULT_VARIABLE_PARAMS
        boundsfilename = os.path.join(FIXTURES_DIR,"JD2_bounds.json")
        with open(boundsfilename) as infh:
            cls.bounds = io.load_bounds(infh, cls.params)
        sampler = samplers.Uniform(cls.bounds, cls.labs, cls.seed)
        cls.reference_seqs = sampler.sequences(cls.numseqs)


    def setUp(self):
        self.sampler = samplers.Uniform(self.bounds, self.labs, self.seed)


    def _compare_dataframes(self, ref, other):
        self.assertTrue(all(ref.index == other.index))
        self.assertTrue(all(ref.columns == other.columns))
        self.assertTrue(np.allclose(ref.to_numpy(), other.to_numpy()))


    def test_roundtrip_jsonlines_iter(self):
        """- Data converted to JSON lines from iter correctly"""
        test_seqs = Sequences.from_iter(itertools.islice(self.sampler, self.numseqs))
        self.assertIsNone(test_seqs.df)

        jsonlines = list(test_seqs.to_jsonlines())
        self.assertEqual(len(jsonlines), self.numseqs)

        recovered_seqs = Sequences.from_iter(map(Sequence.from_json, jsonlines),
                                             slurp=True)
        self._compare_dataframes(self.reference_seqs.df, recovered_seqs.df)


    def test_roundtrip_jsonlines_slurp(self):
        """- Data converted to JSON lines from memory correctly"""
        test_seqs = self.sampler.sequences(self.numseqs)
        self.assertIsNotNone(test_seqs.df)

        jsonlines = list(test_seqs.to_jsonlines())
        self.assertEqual(len(jsonlines), self.numseqs)

        recovered_seqs = Sequences.from_iter(map(Sequence.from_json, jsonlines),
                                             slurp=True)
        self._compare_dataframes(self.reference_seqs.df, recovered_seqs.df)


    def test_roundtrip_netcdf_iter(self):
        """- Data stored to directory of NetCDF files from iter correctly"""
        test_seqs = Sequences.from_iter(itertools.islice(self.sampler, self.numseqs))
        self.assertIsNone(test_seqs.df)

        with TemporaryDirectory() as tmpdir:
            pathname = os.path.join(tmpdir, "test.nc")
            test_seqs.to_netcdf(pathname)
            recovered_seqs = Sequences.from_netcdf(pathname, slurp=True)
            self.assertTrue(os.path.isdir(pathname))
        self._compare_dataframes(self.reference_seqs.df, recovered_seqs.df)


    def test_roundtrip_netcdf_slurp(self):
        """- Data stored to single NetCDF file from in-memory correctly"""
        test_seqs = self.sampler.sequences(self.numseqs)
        self.assertIsNotNone(test_seqs.df)

        with TemporaryDirectory() as tmpdir:
            pathname = os.path.join(tmpdir, "test.nc")
            test_seqs.to_netcdf(pathname)
            recovered_seqs = Sequences.from_netcdf(pathname, slurp=True)
            self.assertTrue(os.path.isfile(pathname))
        self._compare_dataframes(self.reference_seqs.df, recovered_seqs.df)


    def test_random_access(self):
        """- Test random access of Sequences from in-memory and on-disk"""
        with TemporaryDirectory() as tmpdir:
            netcdf_filename = os.path.join(tmpdir, "test_file.nc")
            netcdf_dirname = os.path.join(tmpdir, "test_dir.nc")
            self.reference_seqs.to_netcdf(netcdf_filename)
            Sequences.from_iter(iter(self.reference_seqs)).to_netcdf(netcdf_dirname)
            self.assertTrue(os.path.isfile(netcdf_filename))
            self.assertTrue(os.path.isdir(netcdf_dirname))
            seqs_netcdf_file = Sequences.from_netcdf(netcdf_filename)
            seqs_netcdf_dir = Sequences.from_netcdf(netcdf_dirname)
            for i in range(len(self.reference_seqs)):
                self._compare_dataframes(self.reference_seqs.df.loc[i],
                                         self.reference_seqs[i])
                self._compare_dataframes(self.reference_seqs.df.loc[i],
                                         seqs_netcdf_file[i])
                self._compare_dataframes(self.reference_seqs.df.loc[i],
                                         seqs_netcdf_dir[i])
            for i in range(-len(self.reference_seqs), 0):
                j = len(self.reference_seqs) + i
                self._compare_dataframes(self.reference_seqs.df.loc[j],
                                         self.reference_seqs[i])
                self._compare_dataframes(self.reference_seqs.df.loc[j],
                                         seqs_netcdf_file[i])
                self._compare_dataframes(self.reference_seqs.df.loc[j],
                                         seqs_netcdf_dir[i])


if __name__ == "__main__":
    unittest.main()
