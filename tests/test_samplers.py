# -*- coding: utf-8 -*-

from os import path
import unittest
import itertools
from collections import defaultdict

import pandas as pd
import numpy as np

from evoclearn.core import Sequences
from evoclearn.core import (TARGETS_TIMECONSTANTS_PARAMS,
                            DEFAULT_VARIABLE_PARAMS,
                            CONSONANT_CONTROL_PARAMS,
                            VOCALTRACT_PARAMS)
from evoclearn.core import io
from evoclearn.core import samplers
from evoclearn.core.samplers.tied_targets import (sort_uniq_paramkeys,
                                                  sequence_to_params,
                                                  sequence_to_paramvalues,
                                                  params_to_sequence)


FIXTURES_DIR = path.join(path.dirname(__file__), "fixtures")


class TestUniform(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls.seed = 777
        cls.labs = ["C", "V"]
        cls.params = DEFAULT_VARIABLE_PARAMS
        boundsfilename = path.join(FIXTURES_DIR,"JD2_bounds.json")
        with open(boundsfilename) as infh:
            cls.bounds = io.load_bounds(infh, cls.params)

    def test_reproducibility(self):
        """- Sequences generated one-by-one and in-memory are identical
        given the same seed"""
        numseqs = 50
        # in-memory
        sampler = samplers.Uniform(self.bounds, self.labs, self.seed)
        seqs1 = sampler.sequences(numseqs)
        # generated one-by-one
        sampler = samplers.Uniform(self.bounds, self.labs, self.seed)
        seqs2 = Sequences.from_iter(itertools.islice(sampler, numseqs), slurp=True)
        self.assertTrue(seqs1.df.equals(seqs2.df))

    def test_parameter_ranges(self):
        """- All parameter values are within limits defined in the bounds
        file"""
        sampler = samplers.Uniform(self.bounds, self.labs)
        seqs = sampler.sequences(10000)
        for param in seqs.df.columns:
            ge_min = seqs.df[param] >= self.bounds[param]["min"]
            ge_min = all(ge_min)
            lt_max = seqs.df[param] < self.bounds[param]["max"]
            lt_max = all(lt_max)
            self.assertTrue(ge_min and lt_max)


class TestUniformProbLink(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls.seed = 777
        cls.consonants = ["g", "b"]
        boundsfilename = path.join(FIXTURES_DIR,
                                   "JD2_bounds.json")
        with open(boundsfilename) as infh:
            cls.bounds = io.load_bounds(infh)

    def test_all_params_defined(self):
        """- Defines a complete set of params with no undefined values"""
        sampler = samplers.UniformProbLink(self.bounds,
                                           self.consonants,
                                           self.seed)
        seqs = sampler.sequences(3)
        self.assertEqual(list(seqs.df.columns),
                         list(TARGETS_TIMECONSTANTS_PARAMS))
        self.assertFalse(seqs.df.isnull().values.any())

    def test_correct_parameters_shared(self):
        """- Vowel params are shared with consonants correctly"""
        sampler = samplers.UniformProbLink(self.bounds,
                                           self.consonants,
                                           self.seed)
        seqs = sampler.sequences(3)
        expected_unique_values_by_param_for_gb = defaultdict(int)
        for param in TARGETS_TIMECONSTANTS_PARAMS:
            expected_unique_values_by_param_for_gb[param] += 1
        for param in CONSONANT_CONTROL_PARAMS["velar_stop"]:
            expected_unique_values_by_param_for_gb[param] += 1
        for param in CONSONANT_CONTROL_PARAMS["bilabial_stop"]:
            expected_unique_values_by_param_for_gb[param] += 1
        for seq in seqs:
            for param in TARGETS_TIMECONSTANTS_PARAMS:
                unique_values = len(set(seq[param].values))
                self.assertEqual(expected_unique_values_by_param_for_gb[param],
                                 unique_values)

    def test_expected_covariance_vowels(self):
        """- Probability link sampling causes the expected param covariance"""
        expcovfname = "uniform_problink_V_777_100_expected_cov.json"
        expcovfpath = path.join(FIXTURES_DIR, expcovfname)
        expected_cov = pd.read_json(expcovfpath, orient="table")
        # Sample vowel only:
        sampler = samplers.UniformProbLink(self.bounds, seed=self.seed)
        seqs = sampler.sequences(100)
        recovered_cov = seqs.df.cov()
        # import matplotlib.pyplot as pl
        # pl.matshow(expected_cov)
        # pl.matshow(recovered_cov)
        # pl.show()
        # with open("tmp.json", "w") as outfh:
        #     outfh.write(recovered_cov.to_json(orient="table"))
        self.assertTrue(np.allclose(expected_cov.to_numpy(),
                                    recovered_cov.to_numpy()))

    def test_not_expected_covariance_vowels(self):
        """- Probability link sampling with different reference VT params"""
        expcovfname = "uniform_problink_V_777_100_expected_cov.json"
        expcovfpath = path.join(FIXTURES_DIR, expcovfname)
        expected_cov = pd.read_json(expcovfpath, orient="table")
        # Sample vowel only:
        sampler = samplers.UniformProbLink(self.bounds, seed=self.seed)
        # We change the reference vocaltract params from neutral to max just
        # for the sake of testing that the reference is set internally, in
        # practice we will periodically update the best params after each or
        # some iteration.
        bounds = pd.DataFrame(self.bounds)
        reference_vt_params = bounds.loc["max", list(VOCALTRACT_PARAMS)]
        sampler.best_vowel_vocaltract_params = reference_vt_params
        seqs = sampler.sequences(100)
        recovered_cov = seqs.df.cov()
        self.assertFalse(np.allclose(expected_cov.to_numpy(),
                                     recovered_cov.to_numpy()))


class TestTiedTargets(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls.seed = 777
        boundsfilename = path.join(FIXTURES_DIR,
                                   "JD2_bounds.json")
        with open(boundsfilename) as infh:
            speaker_bounds = io.load_bounds(infh)
        cls.baseseq = io.default_sequence_from_bounds(speaker_bounds,
                                                      labels=["C", "V"])
        cls.variable_param_list = ["C-HX", "C-JA", "V-HX", "V-HY"]     
        cls.variable_param_bounds = {k: speaker_bounds[k.split("-")[1]]
                                     for k in cls.variable_param_list}
        cls.seqs = samplers.TiedTargets(cls.variable_param_bounds,
                                        cls.baseseq,
                                        seed=cls.seed).sequences(10)
        cls.seqs_copy_v = samplers.TiedTargets(cls.variable_param_bounds,
                                               cls.baseseq,
                                               copy="V",
                                               seed=cls.seed).sequences(10)

    def test_sort_uniq_paramkeys(self):
        """- Uniqify and sort flat "paramkeys" by label order and standard param order"""
        test_input = ["V-HY", "V-HX", "C-JA", "C-HX", "V-HY", "C-JA"]
        self.assertEqual(sort_uniq_paramkeys(test_input),
                         self.variable_param_list)
        self.assertEqual(sort_uniq_paramkeys(test_input, lab_order=["V", "C"]),
                         (self.variable_param_list[2:] +
                          self.variable_param_list[:2]))

    def test_sequence_to_params(self):
        """- Conversion from Sequence to params dict and paramvals list"""
        for seq in self.seqs:
            params = sequence_to_params(seq, self.variable_param_bounds)
            self.assertEqual(len(params), len(self.variable_param_bounds))
            for paramkey in self.variable_param_bounds:
                lab, param = paramkey.split("-")
                self.assertEqual(params[paramkey],
                                float(seq.loc[pd.IndexSlice[:, lab], param]))
            paramvals = sequence_to_paramvalues(seq, self.variable_param_bounds)
            self.assertEqual(paramvals, [params[k] for k in self.variable_param_bounds])

    def test_params_to_sequence(self):
        """- Conversion from params dict to Sequence"""
        for seq in self.seqs:
            params = sequence_to_params(seq, self.variable_param_bounds)
            sseq = params_to_sequence(params, self.baseseq)
            self.assertTrue((seq == sseq).to_numpy().all())
            sseq = params_to_sequence(params)
            self.assertEqual(set([pk.split("-")[1] for pk in self.variable_param_bounds]),
                             set(sseq.columns))
            self.assertEqual(set([pk.split("-")[0] for pk in self.variable_param_bounds]),
                             set(sseq.index.get_level_values("lab")))
            arr = sseq.to_numpy().flatten()
            total_num_seq_vals = len(sseq.columns) * len(set(sseq.index.get_level_values("lab")))
            num_nans = len(arr[np.isnan(arr)])
            self.assertEqual(num_nans + len(params), total_num_seq_vals)

        for seq in self.seqs_copy_v:
            params = sequence_to_params(seq, self.variable_param_bounds)
            sseq = params_to_sequence(params, self.baseseq, copy="V")
            self.assertTrue((seq == sseq).to_numpy().all())

    def test_sampled_tied_values(self):
        """- Sampled values shared with base and copied correctly"""
        base_params = sequence_to_params(self.baseseq)
        #Copied V
        should_differ = ["JA", "HX"]
        should_differ_from_base = self.variable_param_list + ["C-HY"]
        for seq in self.seqs_copy_v:
            params = sequence_to_params(seq)
            for p in should_differ:
                self.assertNotEqual(params[f"C-{p}"], params[f"V-{p}"])
            for p in set(seq.columns).difference(should_differ):
                self.assertEqual(params[f"C-{p}"], params[f"V-{p}"])
            for pk in should_differ_from_base:
                self.assertNotEqual(params[pk], base_params[pk])
            for pk in set(base_params).difference(should_differ_from_base):
                self.assertEqual(params[pk], base_params[pk])
        #No copy
        should_differ = ["JA", "HX", "HY"]
        should_differ_from_base = self.variable_param_list
        for seq in self.seqs:
            params = sequence_to_params(seq)
            for p in should_differ:
                self.assertNotEqual(params[f"C-{p}"], params[f"V-{p}"])
            for p in set(seq.columns).difference(should_differ):
                self.assertEqual(params[f"C-{p}"], params[f"V-{p}"])
            for pk in should_differ_from_base:
                self.assertNotEqual(params[pk], base_params[pk])
            for pk in set(base_params).difference(should_differ_from_base):
                self.assertEqual(params[pk], base_params[pk])
                
    def test_sampled_values_in_correct_ranges(self):
        """- Sampled values in correct ranges"""
        for pk in self.variable_param_bounds:
            lab, param = pk.split("-")
            idx = pd.IndexSlice[:, :, lab]
            arr = self.seqs.df.loc[idx, param].to_numpy()
            self.assertTrue(np.all(arr >= self.variable_param_bounds[pk]["min"]))
            self.assertTrue(np.all(arr < self.variable_param_bounds[pk]["max"]))


if __name__ == "__main__":
    unittest.main()
