# -*- coding: utf-8 -*-

import os
import unittest
import itertools
import functools
import json

import numpy as np
import pandas as pd

from evoclearn.core import io
from evoclearn.core import Sequence, Sequences
from evoclearn.core import definitions as defs
from evoclearn.core.samplers import Uniform
from evoclearn.core.mappings import normalise_minmax, normalise_standard


FIXTURES_DIR = os.path.join(os.path.dirname(__file__), "fixtures")


class TestMappings(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls.seed = 777
        numseqs = 20
        boundsfilename = os.path.join(FIXTURES_DIR,"JD2_bounds.json")
        with open(boundsfilename) as infh:
            cls.bounds = io.load_bounds(infh)
        sampler = Uniform(cls.bounds, seed=cls.seed)
        samples = itertools.islice(sampler, numseqs)
        cls.seqs = Sequences.from_iter(samples, slurp=True)
        # Constructed test stats file with means and stddevs related to the
        # bounds so that the result of normalise_standard should give the same
        # values as normalise_minmax
        statsfilename = os.path.join(FIXTURES_DIR, "JD2_stats.json")
        with open(statsfilename) as infh:
            cls.stats = json.load(infh)


    def test_normalise_minmax(self):
        """- Normalise parameters in a sequence to fixed range"""
        # Default new range is [-1.0, 1.0]
        normf = functools.partial(normalise_minmax, bounds=self.bounds)
        normseqs = Sequences.from_iter(map(normf, self.seqs), slurp=True)
        arr = normseqs.df.to_numpy()
        self.assertFalse(np.all(arr >= 0))
        self.assertTrue(np.all(np.logical_and(arr >= -1.0, arr <= 1.0)))
        # Specify new range: [0.0, 1.0]
        normf = functools.partial(normalise_minmax,
                                  bounds=self.bounds,
                                  lb=0.0,
                                  ub=1.0)
        normseqs = Sequences.from_iter(map(normf, self.seqs), slurp=True)
        arr = normseqs.df.to_numpy()
        self.assertTrue(np.all(np.logical_and(arr >= 0.0, arr <= 1.0)))


    def test_normalise_minmax_inverse(self):
        """- Normalise (min/max) inverse"""
        normf = functools.partial(normalise_minmax, bounds=self.bounds)
        invnormf = functools.partial(normalise_minmax, bounds=self.bounds, inverse=True)
        unnormed_seqs = Sequences.from_iter(map(invnormf,
                                                map(normf,
                                                    self.seqs)),
                                            slurp=True)
        arr = unnormed_seqs.df.to_numpy()
        expected_arr = self.seqs.df.to_numpy()
        self.assertTrue(np.allclose(expected_arr, arr))


    def test_normalise_minmax_returns_input_type(self):
        """- Normalise (min/max) returns input dataframe type"""
        seq = self.seqs[0]
        seqdf = pd.DataFrame(seq)
        self.assertTrue(type(seq) is Sequence)
        self.assertTrue(type(seqdf) is pd.DataFrame)
        self.assertTrue(type(normalise_minmax(seq,
                                              self.bounds)) is Sequence)
        self.assertTrue(type(normalise_minmax(seqdf,
                                              self.bounds)) is pd.DataFrame)
        self.assertTrue(type(normalise_minmax(normalise_minmax(seq,
                                                               self.bounds),
                                              self.bounds,
                                              inverse=True)) is Sequence)
        self.assertTrue(type(normalise_minmax(normalise_minmax(seqdf,
                                                               self.bounds),
                                              self.bounds,
                                              inverse=True)) is pd.DataFrame)


    def test_normalise_minmax_works_around_nans(self):
        """- Normalise (min/max) works around NaNs"""
        vtbounds = {k: v for k, v in self.bounds.items() if k in defs.VOCALTRACT_PARAMS}
        cvseq = next(Uniform(vtbounds, labels=["C", "V"], seed=self.seed))
        normseq = normalise_minmax(cvseq, self.bounds)
        ref_normseq_with_nans = Sequence(normseq.copy())
        ref_normseq_with_nans.loc[(0, "C"), "HX"] = np.nan
        ref_normseq_with_nans.loc[(1, "V"), "HY"] = np.nan
        cvseq_with_nans = Sequence(cvseq.copy())
        cvseq_with_nans.loc[(0, "C"), "HX"] = np.nan
        cvseq_with_nans.loc[(1, "V"), "HY"] = np.nan
        normseq_with_nans = normalise_minmax(cvseq_with_nans,
                                             self.bounds)
        unnormseq_with_nans = normalise_minmax(normseq_with_nans,
                                               self.bounds,
                                               inverse=True)
        self.assertTrue(np.allclose(cvseq_with_nans.to_numpy(),
                                    unnormseq_with_nans.to_numpy(),
                                    equal_nan=True))
        self.assertTrue(np.allclose(ref_normseq_with_nans.to_numpy(),
                                    normseq_with_nans.to_numpy(),
                                    equal_nan=True))


    def test_normalise_standard(self):
        """- Normalise parameters by subtracting mean and dividing by stddev"""
        # Create reference with normalise_minmax
        normf = functools.partial(normalise_minmax, bounds=self.bounds)
        normseqs = Sequences.from_iter(map(normf, self.seqs), slurp=True)
        ref_arr = normseqs.df.to_numpy()
        # Apply normalise_standard
        normf = functools.partial(normalise_standard, stats=self.stats)
        normseqs = Sequences.from_iter(map(normf, self.seqs), slurp=True)
        arr = normseqs.df.to_numpy()
        self.assertTrue(np.allclose(ref_arr, arr))


    def test_normalise_standard_inverse(self):
        """- Normalise (standard) inverse"""
        normf = functools.partial(normalise_standard, stats=self.stats)
        invnormf = functools.partial(normalise_standard, stats=self.stats, inverse=True)
        unnormed_seqs = Sequences.from_iter(map(invnormf,
                                                map(normf,
                                                    self.seqs)),
                                            slurp=True)
        arr = unnormed_seqs.df.to_numpy()
        expected_arr = self.seqs.df.to_numpy()
        self.assertTrue(np.allclose(expected_arr, arr))


    def test_normalise_standard_returns_input_type(self):
        """- Normalise (standard) returns input dataframe type"""
        seq = self.seqs[0]
        seqdf = pd.DataFrame(seq)
        self.assertTrue(type(seq) is Sequence)
        self.assertTrue(type(seqdf) is pd.DataFrame)
        self.assertTrue(type(normalise_standard(seq,
                                                self.stats)) is Sequence)
        self.assertTrue(type(normalise_standard(seqdf,
                                                self.stats)) is pd.DataFrame)
        self.assertTrue(type(normalise_standard(normalise_standard(seq,
                                                                   self.stats),
                                                self.stats,
                                                inverse=True)) is Sequence)
        self.assertTrue(type(normalise_standard(normalise_standard(seqdf,
                                                                   self.stats),
                                                self.stats,
                                                inverse=True)) is pd.DataFrame)


    def test_normalise_standard_works_around_nans(self):
        """- Normalise (standard) works around NaNs"""
        vtbounds = {k: v for k, v in self.bounds.items() if k in defs.VOCALTRACT_PARAMS}
        cvseq = next(Uniform(vtbounds, labels=["C", "V"], seed=self.seed))
        normseq = normalise_standard(cvseq, self.stats)
        ref_normseq_with_nans = Sequence(normseq.copy())
        ref_normseq_with_nans.loc[(0, "C"), "HX"] = np.nan
        ref_normseq_with_nans.loc[(1, "V"), "HY"] = np.nan
        cvseq_with_nans = Sequence(cvseq.copy())
        cvseq_with_nans.loc[(0, "C"), "HX"] = np.nan
        cvseq_with_nans.loc[(1, "V"), "HY"] = np.nan
        normseq_with_nans = normalise_standard(cvseq_with_nans,
                                               self.stats)
        unnormseq_with_nans = normalise_standard(normseq_with_nans,
                                                 self.stats,
                                                 inverse=True)
        self.assertTrue(np.allclose(cvseq_with_nans.to_numpy(),
                                    unnormseq_with_nans.to_numpy(),
                                    equal_nan=True))
        self.assertTrue(np.allclose(ref_normseq_with_nans.to_numpy(),
                                    normseq_with_nans.to_numpy(),
                                    equal_nan=True))


if __name__ == "__main__":
    unittest.main()
