# -*- coding: utf-8 -*-

import os
from tempfile import TemporaryDirectory
import unittest

import numpy as np

from evoclearn.core import Track, Tracks


FIXTURES_DIR = os.path.join(os.path.dirname(__file__), "fixtures")


class TestTracks(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        with open(os.path.join(FIXTURES_DIR, "feats.jsonlines")) as infh:
            cls.ref_tracks_list = [Track.from_json(line) for line in infh]


    def _compare_dataframes(self, ref, other):
        self.assertTrue(all(ref.index == other.index))
        self.assertTrue(all(ref.columns == other.columns))
        self.assertTrue(np.allclose(ref.to_numpy(), other.to_numpy()))


    def test_roundtrip_jsonlines_iter(self):
        """- Data converted to JSON lines from iter correctly"""
        ref_tracks_list = [t.copy() for t in self.ref_tracks_list]
        
        test_tracks = Tracks.from_iter(iter(ref_tracks_list))
        self.assertIsNone(test_tracks.df)

        jsonlines = list(test_tracks.to_jsonlines())
        self.assertEqual(len(jsonlines), len(ref_tracks_list))

        recovered_tracks = Tracks.from_iter(map(Track.from_json, jsonlines))
        for t, reft in zip(recovered_tracks, ref_tracks_list):
            self._compare_dataframes(t, reft)


    def test_roundtrip_jsonlines_slurp(self):
        """- Data converted to JSON lines from memory correctly"""
        ref_tracks_list = [t.copy() for t in self.ref_tracks_list]
        
        test_tracks = Tracks.from_iter(iter(ref_tracks_list), slurp=True)
        self.assertIsNotNone(test_tracks.df)

        jsonlines = list(test_tracks.to_jsonlines())
        self.assertEqual(len(jsonlines), len(ref_tracks_list))

        recovered_tracks = Tracks.from_iter(map(Track.from_json, jsonlines), slurp=True)
        self._compare_dataframes(recovered_tracks.df, test_tracks.df)


    def test_roundtrip_netcdf_iter(self):
        """- Data stored to directory of NetCDF files from iter correctly"""
        ref_tracks_list = [t.copy() for t in self.ref_tracks_list]

        test_tracks = Tracks.from_iter(iter(ref_tracks_list))
        self.assertIsNone(test_tracks.df)

        with TemporaryDirectory() as tmpdir:
            pathname = os.path.join(tmpdir, "test.nc")
            test_tracks.to_netcdf(pathname)
            recovered_tracks = Tracks.from_netcdf(pathname, slurp=True)
            self.assertTrue(os.path.isdir(pathname))
        self._compare_dataframes(Tracks.from_iter(iter(ref_tracks_list), slurp=True).df,
                                 recovered_tracks.df)


    def test_roundtrip_netcdf_slurp(self):
        """- Data stored to single NetCDF file from in-memory correctly"""
        ref_tracks_list = [t.copy() for t in self.ref_tracks_list]

        test_tracks = Tracks.from_iter(iter(ref_tracks_list), slurp=True)
        self.assertIsNotNone(test_tracks.df)

        with TemporaryDirectory() as tmpdir:
            pathname = os.path.join(tmpdir, "test.nc")
            test_tracks.to_netcdf(pathname)
            recovered_tracks = Tracks.from_netcdf(pathname, slurp=True)
            self.assertTrue(os.path.isfile(pathname))
        self._compare_dataframes(test_tracks.df, recovered_tracks.df)


    def test_random_access(self):
        """- Test random access of Sequences from in-memory and on-disk"""
        ref_tracks_list = [t.copy() for t in self.ref_tracks_list]
        ref_tracks = Tracks.from_iter(iter(ref_tracks_list), slurp=True)

        with TemporaryDirectory() as tmpdir:
            netcdf_filename = os.path.join(tmpdir, "test_file.nc")
            netcdf_dirname = os.path.join(tmpdir, "test_dir.nc")
            ref_tracks.to_netcdf(netcdf_filename)
            Tracks.from_iter(iter(ref_tracks_list)).to_netcdf(netcdf_dirname)
            self.assertTrue(os.path.isfile(netcdf_filename))
            self.assertTrue(os.path.isdir(netcdf_dirname))
            trks_netcdf_file = Tracks.from_netcdf(netcdf_filename)
            trks_netcdf_dir = Tracks.from_netcdf(netcdf_dirname)
            for i in range(len(ref_tracks)):
                self._compare_dataframes(ref_tracks.df.loc[i], ref_tracks[i])
                self._compare_dataframes(ref_tracks.df.loc[i], trks_netcdf_file[i])
                self._compare_dataframes(ref_tracks.df.loc[i], trks_netcdf_dir[i])
            for i in range(-len(ref_tracks), 0):
                j = len(ref_tracks) + i
                self._compare_dataframes(ref_tracks.df.loc[j], ref_tracks[i])
                self._compare_dataframes(ref_tracks.df.loc[j], trks_netcdf_file[i])
                self._compare_dataframes(ref_tracks.df.loc[j], trks_netcdf_dir[i])


if __name__ == "__main__":
    unittest.main()
