# -*- coding: utf-8 -*-

import os
import unittest
import itertools
import json

import pandas as pd

from evoclearn.core import Sequence
from evoclearn.core import mappings
from evoclearn.core import io
from evoclearn.core import samplers
from evoclearn.core import vocaltractlab as vtl


FIXTURES_DIR = os.path.join(os.path.dirname(__file__), "fixtures")


def neutral_seq(bounds):
    """The neutral values are actually in the bounds file, this is just
    convenience to create a single target V Sequence from that..."""
    labels = "V"
    seq = next(samplers.Uniform(bounds, labels=list(labels)))
    cols = list(bounds)
    seq.loc[:, cols] = [bounds[c]["default"] for c in cols]
    return seq


class TestVocalTractLab(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        boundsfilename = os.path.join(FIXTURES_DIR,"JD2_bounds.json")
        with open(boundsfilename) as infh:
            cls.bounds = io.load_bounds(infh)
        
    def test_synth_neutral(self):
        segdurs = [0.500]
        seq = neutral_seq(self.bounds)
        qta = mappings.add_slope_and_duration(seq, segdurs)
        curves = mappings.target_approximation(qta)

        speakerfilename = os.path.join(FIXTURES_DIR, "JD2_speaker.xml")
        vtl.initialise(speakerfilename)
        wav = vtl.synthesise(curves)

        # print(wav)
        # import matplotlib.pyplot as pl
        # pl.figure()
        # pl.plot(wav)
        # pl.show()
        # io.wav_write(wav, "manual.wav")



if __name__ == "__main__":
    unittest.main()
