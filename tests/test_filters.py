# -*- coding: utf-8 -*-

import os
import unittest
import itertools
import functools

from evoclearn.core import io
from evoclearn.core import Sequences
from evoclearn.core.constraints import simple_constraints, all_constraints
from evoclearn.core.mappings import satisfies_constraints
from evoclearn.core.filters import discard_by_constraints
from evoclearn.core.samplers import UniformProbLink
import evoclearn.core.vocaltractlab as vtl


FIXTURES_DIR = os.path.join(os.path.dirname(__file__), "fixtures")


class TestFilters(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls.seed = 777
        cls.numseqs = 100
        boundsfilename = os.path.join(FIXTURES_DIR,"JD2_bounds.json")
        with open(boundsfilename) as infh:
            cls.bounds = io.load_bounds(infh)
        vtl.initialise(os.path.join(FIXTURES_DIR, "JD2_speaker.xml"))
        
    
    def test_discard_by_simple_constraints(self):
        """- Discards sequences that do not satisfy simple constraints"""
        sampler = UniformProbLink(self.bounds, seed=self.seed)
        seqs = Sequences.from_iter(itertools.islice(sampler,
                                                    self.numseqs), slurp=True)
        invalid_seqs = 0
        for seq in seqs:
            if not satisfies_constraints(seq, simple_constraints):
                invalid_seqs += 1
        filtered_seqs = Sequences.from_iter(discard_by_constraints(seqs),
                                            slurp=True)
        self.assertTrue(invalid_seqs > 0)
        self.assertEqual(len(filtered_seqs), len(seqs) - invalid_seqs)
        invalid_seqs = 0
        for seq in filtered_seqs:
            if not satisfies_constraints(seq, simple_constraints):
                invalid_seqs += 1
        self.assertEqual(invalid_seqs, 0)


    def test_discard_by_all_constraints(self):
        """- Discards sequences that do not satisfy constraints & voicing"""
        sampler = UniformProbLink(self.bounds, seed=self.seed)
        seqs = Sequences.from_iter(itertools.islice(sampler,
                                                    self.numseqs), slurp=True)
        invalid_seqs = 0
        for seq in seqs:
            if not satisfies_constraints(seq, all_constraints):
                invalid_seqs += 1
        discardf = functools.partial(discard_by_constraints,
                                     constraints=all_constraints)
        filtered_seqs = Sequences.from_iter(discardf(seqs), slurp=True)
        self.assertTrue(invalid_seqs > 0)
        self.assertEqual(len(filtered_seqs), len(seqs) - invalid_seqs)
        invalid_seqs = 0
        for seq in filtered_seqs:
            if not satisfies_constraints(seq, simple_constraints):
                invalid_seqs += 1
        self.assertEqual(invalid_seqs, 0)
        

if __name__ == "__main__":
    unittest.main()
