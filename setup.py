#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
from os.path import join as pjoin
import shutil
import subprocess
import platform
from glob import glob
from tempfile import TemporaryDirectory

from setuptools.command.build_py import build_py
from setuptools import setup, find_namespace_packages, Extension


WORKING_PATH = os.getcwd()


class BuildLinuxVtlApi(build_py):
    """Build VocalTractLab API"""
    def run(self):
        print("Building VocalTractLab API...")
        os.chdir("src")
        with TemporaryDirectory() as tmpdir:
            subprocess.check_call(["tar",
                                   "xf",
                                   "VTL-API.tar.bz2",
                                   "--strip-components=1",
                                   "--directory",
                                   tmpdir])
            os.chdir(tmpdir)
            subprocess.check_call(["make","-j12"])
            shutil.copy(pjoin("lib", "VocalTractLabApi.so"),
                        pjoin(WORKING_PATH, "evoclearn", "core"))
        os.chdir(WORKING_PATH)
        build_py.run(self)


class SetupWindowsVtlApi(build_py):
    """Copy pre-compiled VocalTractLab API in place"""
    def run(self):
        for fn in glob(pjoin(WORKING_PATH, "src", "win_dll", "*.dll")):
            shutil.copy(fn, pjoin(WORKING_PATH, "evoclearn", "core"))
        build_py.run(self)


with open(pjoin("evoclearn", "core", "version.py")) as infh:
    version = {}
    exec(infh.read(), version)
    version = version["__version__"]


python_requires = ">=3.7"

install_requires = [
    "scipy",
    "numpy>=1.21.0,<1.22.0",
    "pandas",
    "xarray",
    "h5py",
    "netCDF4",
    "dask",
    "toolz",
    "librosa>=0.9.2,<0.10.0",
    "praatio>=5.1.1,<5.2.0",
    "click"
]

extras_require = {
    "dev": ["ipython",
            "matplotlib",
            "ipykernel",
            "nbconvert",
            "jupyter"]
}


if platform.system() == "Windows":
    bits = platform.architecture()[0]
    if not "64" in bits:
        raise Exception("Windows platform needs to be 64-bit...")
    package_data = {"": ["VocalTractLabApi.dll",
                         "vcruntime140.dll",
                         "vcruntime140_1.dll"]}
    cmdclass = {"build_py": SetupWindowsVtlApi}
    ext_modules = None
else:
    package_data = {"": ["VocalTractLabApi.so"]}
    cmdclass = {"build_py": BuildLinuxVtlApi}
    ext_modules = None #[Extension(name="dummy", sources=[])]


setup_args = {
    "name": "evoclearn-core",
    "version": version,
    "description": "Core tools for Early Vocal Learning simulation.",
    "long_description": "Core tools for Early Vocal Learning simulation..",
    "author": "The EVocLearn Group",
    "author_email": "daniel.r.vanniekerk@gmail.com",
    "url": "https://gitlab.com/evoc-learn-group/evoc-learn-core",
    "packages": find_namespace_packages(include=["evoclearn.*"]),
    "python_requires": python_requires,
    "install_requires": install_requires,
    "extras_require": extras_require,
    "cmdclass": cmdclass,
    "package_data": package_data,
    "ext_modules": ext_modules,
    "test_suite": "tests",
    "entry_points": {
        "console_scripts": [
            "evl_sample=evoclearn.core.cli.sample:main",
            "evl_filter=evoclearn.core.cli.filter:main",
            "evl_map=evoclearn.core.cli.map:main",
    ]
    },
    "data_files": [("etc", glob(pjoin("etc", "*"))),
                   ("examples", [f for f in glob(pjoin("examples", "*"))
                                 if os.path.isfile(f)]),
                   ("examples/data", [f for f in glob(pjoin("examples", "data", "*"))
                                      if os.path.isfile(f)])]
}

setup(**setup_args)
