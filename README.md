Evoclearn-core
==============

This Python package provides a set of functions and file formats for implementing VocalTractLab (VTL) processing pipelines, including *articulatory sampling*, *target approximation*, *speech synthesis*, and *acoustic feature extraction*. All of these can be accessed as Python functions or command line programs.

This package is intended to be used for implementing low-level VTL experiments directly **OR** as a core library for other downstream packages such as [evoc-learn-opt](https://gitlab.com/evoc-learn-group/evoc-learn-opt) which implements goal-directed vocal exploration.

If you use these tools, please cite:

```
@inproceedings{xu22_evoclearn,
  title = {{Evoc-Learn -- High quality simulation of early vocal learning}},
  author = {Xu, Y. and Xu, A. and Van Niekerk, D. R. and Gerazov, B. and Birkholz, P. and Krug, P. B. and Prom-on, S. and Halliday, L. F.},
  booktitle = {{Proc. Interspeech}},
  year = {2022},
  month = sep,
  address = {Incheon, South Korea},
  pages = {3665--3666}
}
```

#### Motivation

 - Make it easier to reproduce experiments and make systematic changes to experiments by reusing some components, adding new ones and just redefining the processing pipeline.
 - Have a collection of stable code which is properly versioned and can be reused in individual experiments.
 - Have equivalent Python and Command Line Interfaces for easy use in *Python notebooks*, other sub-projects and computer clusters.

#### Features

 - **Sequence** and **Sequences** classes which manage IO and building up of VTL targets. This allows sampling and passing along incomplete target sequences and gradually filling out all parameters until all parameters required for target approximation is defined.

 - Stateless *mappings* and *filters* with simple input/output interfaces can be applied to target sequences and other objects. This has a few advantages:
    1. Simple interfaces make it easy to change an experimental setup/processing pipeline.
    2. It is sometimes necessary to reproduce part of the processing pipeline in a different context (e.g. when passing a constraint function to an external optimiser).
    3. It is conceptually simple to replace parts of the pipeline with a learnt mapping function such as a neural network.
 - The build script can build VocalTractLab and produce a standalone binary package which can easily be used in different environments.
 - Implementation of tests for components allow us to improve the implementation of some components while ensuring correctness of results.


## Getting started

#### Installation

Install the latest release version from the *Python Package Index* with:

```bash
pip install evoclearn-core
```

Or build a package from this source repository and install with:

```bash
python setup.py bdist_wheel
pip install dist/evoclearn_core-0.18.6-py3-none-any.whl
```

You will need Python [setuptools](https://pypi.org/project/setuptools/), [wheel](https://pypi.org/project/wheel/) and a *C++11 compiler* to build locally.


#### Quickstart

There are some self-documenting Python Notebooks in [examples](https://gitlab.com/evoc-learn-group/evoc-learn-core/-/tree/master/examples) which illustrate how to implement some basic VTL tasks such as defining articulatory targets, speech synthesis and visualisation.
